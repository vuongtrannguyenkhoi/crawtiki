# -*- coding: utf-8 -*-
import scrapy
import re
from scrapy.spiders import Spider
from crawtiki.items import CrawtikiItem
from scrapy.http    import Request
from scrapy.linkextractor import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
class DatatikiSpider(CrawlSpider):
    name = 'datatiki'
    allowed_domains = ['tiki.vn']
    start_urls = ['https://tiki.vn/']
    pattern = re.compile(r'-p[0-9]')
    rules = [
        Rule(
            LinkExtractor(
                canonicalize=True,
                unique=True
            ),
            follow=True,
            callback="parse_items"
        )
    ]
    def parse_items(self, response):
        # items = []
        # links = response.css('a::attr("href")').extract()
        # We stored already crawled links in this list
        # yield response.url
        # item =  CrawtikiItem()
        # item['link'] = response.url
        # return item
        # if self.pattern.search(response.url):
        #     item =  CrawtikiItem()
        #     item['link'] = response.url
        #     yield item
        if self.allowed_domains[0] in response.url and self.pattern.search(response.url):
            item =  CrawtikiItem()
            item['link'] = response.url
            return item
        # Pattern to check proper link
        # I only want to get the tutorial posts
        # linkPattern = re.compile("^\/tutorials\?page=\d+")
    
        # for link in links:
        #     _link = link
        #     if not self.start_urls[0] in _link:
        #         _link = self.start_urls[0] + _link
        #     # If it is a proper link and is not checked yet, yield it to the Spider
        #     if not _link in crawledLinks and self.allowed_domains[0] in _link:
        #         # link = "http://code.tutsplus.com" + link
        #         # yield _link
        #         crawledLinks.append(_link)
        #         if self.pattern.search(_link):
        #             item = CrawtikiItem()
        #             item['link'] = _link
        #             yield item
        #         yield response.follow(_link, callback=self.parse)

        
        
